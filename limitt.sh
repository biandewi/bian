#!/bin/bash

NUM_CPU_CORES=$(nproc --all)

cpulimit -e "wesboh" -l $((65 * $NUM_CPU_CORES))